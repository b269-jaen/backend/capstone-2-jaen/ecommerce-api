const User = require("../models/User");
const mongoose = require("mongoose");
const bcryptjs = require("bcryptjs");

const auth = require("../auth");



// User registration
module.exports.registerUser = (reqBody) => {

	// find user in the db by email
	return User.find({email: reqBody.email}).then(result => {
		// Check if user already exists
		if(result.length > 0){
			return false
		// If not, create new user
		}else {
			let newUser = new User({
				email: reqBody.email,
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				mobileNo: reqBody.mobileNo,
				password: bcryptjs.hashSync(reqBody.password,10)
			});
			return newUser.save().then((user, error) => {
				if(error){
					return false;
				}else{
					return true;
				}
			});
		}

	});
};


// User authentication
module.exports.loginUser = (reqBody) => {
	// find user in the db by email
	return User.findOne({email: reqBody.email}).then(result => {
		// if user is not registered, return message
		if (result == null){
			return false;
		// else if user is registered, compare encrypt password
		}else{
			const isPasswordCorrect = bcryptjs.compareSync(reqBody.password, result.password)
			// if password is match, create access token for the user
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			// If password do not match return message
			}else{
				return true;
			};
		};
	});
};



// Set user as admin using email
module.exports.giveAdmin = (data) => {


	// if the decoded auth token "isAdmin" field is true, change a user's role to admin.
	if(data.isAdmin){
		// find by email and update isAdmin field to true
		return User.findOneAndUpdate({email: data.user.email}, {isAdmin: true}).then((update, err) => {
			if(err){
				return "No user found"
			}else{
				return "User is now an admin"
			}
		});
	}

	// if auth key is not admin, return resolve promise
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});

};

// Get all registered users
module.exports.showAll = (data) => {
	// if the decoded auth token "isAdmin" field is true, show all user
	if(data.isAdmin){
		return User.find().then((users, err) => {
			if(err){
				return "No user found"
			}else{
				return users;
			}
		});
	};

	// if auth key is not admin, return resolve promise
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

// Retreive user details
module.exports.getProfile = (userData) => {
	return User.findById(userData).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};