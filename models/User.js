const mongoose = require("mongoose");

let userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "User email is required!"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin: {
		type : Boolean,
		default : false
	},
	firstName: {
		type: String,
		required: [true, "First name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	mobileNo: {type: String}
});



module.exports = mongoose.model("User", userSchema);
