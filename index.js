// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Routes
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const cartRoute = require("./routes/cartRoute");

// Server
const app = express();

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
// localhost:3000/users
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);
app.use("/cart", cartRoute);


// Database Connection 
mongoose.connect("mongodb+srv://nathanieljaen12:admin123@zuitt-bootcamp.vs10cer.mongodb.net/EcommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log('Now connected to the cloud database'));


// Server listening
// Will use the defined port number for the application whenever environment variable is available or used port 3000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 3001, () => console.log(`Now connected to port ${process.env.PORT || 3001}`) )
 
