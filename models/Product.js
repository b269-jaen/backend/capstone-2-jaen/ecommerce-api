const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product name is required"]
	},
	description : {
		type : String
	},
	price : {
		type : Number,
		required : [true, "Produc price is required"]
	},
	isActive : {
		type : Boolean,
		default : true 
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	note: {
		type: String,
		default: "Recently On Sale"
	},
	img: {
		type: String,
		default: null
	}
});

module.exports = mongoose.model("Product", productSchema)