const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
const auth = require("../auth");


// add products to cart
router.post("/add", auth.verify, (req, res) => {
	const data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	body: req.body
	}
	cartController.addToCart(data).then(resultFromController => res.send(resultFromController));
});

// remove product from the cart
router.post("/remove", auth.verify, (req, res) => {
	const data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	body: req.body
	}

	cartController.removeFromCart(data).then(resultFromController => res.send(resultFromController));
});

// create Order from Cart
router.get("/checkOut", auth.verify, (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		body: req.body
		}

	cartController.checkOut(data).then(resultFromController => res.send(resultFromController));
})

// Get user's cart
router.get("/show", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		body: req.body
	}
	cartController.showCart(data).then(resultFromController => res.send(resultFromController));
})

// Empty products in the cart
router.delete("/empty", auth.verify, (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	cartController.removeAll(data).then(resultFromController => res.send(resultFromController));
})


// exports router
module.exports = router;