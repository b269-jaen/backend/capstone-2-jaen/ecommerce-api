// Dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// route for user registration
router.post("/register", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// route for setting user to admin using user email
router.post("/admin/set", auth.verify, (req, res) => {
	const data = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	userController.giveAdmin(data).then(resultFromController => res.send(resultFromController));
})

// route for getting all registered users (admin only)
router.get("/all", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	
	userController.showAll(data).then(resultFromController => res.send(resultFromController));
})


router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization).id;

// Provides the user's ID for the getProfile controller method
userController.getProfile(userData).then(resultFromController => res.send(resultFromController));
});


// export route
module.exports = router;