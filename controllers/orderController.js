const Order = require("../models/Order");
const mongoose = require("mongoose");

const Product = require("../models/Product");


// Non-admin User checkout
module.exports.createOrder = async (data) => {
  // if auth key isAdmin = true, dont proceed
  if (data.isAdmin) {
    return 'User must NOT be an admin to access this!';
  }

  // create order from the request body
  const order = new Order({
    userId: data.userId,
    products: []
  });

  // initialize totalAmount
  let totalAmount = 0;

    // find Product by its id
    const foundProduct = await Product.findById(data.body.productId);
    // Add product to the Order collection
    const addProduct = {
      productId: foundProduct._id,
      productName: foundProduct.name,
      quantity: data.body.quantity,
      subTotal: data.body.quantity * foundProduct.price,
      img: foundProduct.img
    };
    // addition assignment to total amount from subTotal
    totalAmount += addProduct.subTotal;
    order.products.push(addProduct);
  

  // set total amount from all the products in the array
  order.totalAmount = totalAmount
  // save Order created
  return order.save().then((savedOrder, err) => {
    if(err){
      return false;
    }else{
      return savedOrder;
    }
  });
};



// Retrieve authenticated user's orders
module.exports.getUserOrder = (data) => {
  return Order.find({userId: data.id}).then(result =>{
    return result;
  })
}



// Retrieve all orders (Admin only)
module.exports.getAllOrders = (isAdmin) => {
  // if auth key isAdmin: true, get all users order
	if(isAdmin){
		return Order.find({}).then(result => {return result});
	};

  // if isAdmin: false, return resolve message.
	let message = Promise.resolve('User must be an admin to access this!');
	return message.then((value) => {
		return {value};
	});

}

// Retrieve pending orders
module.exports.getPending = (isAdmin) => {
  if(isAdmin){
    return Order.find({status: 'PENDING'}).then(result => {return result});
  }

  let message = Promise.resolve('User must be an admin to access this!');
  return message.then((value) => {
    return {value};
  });
}

// Retrieve approved orders
module.exports.getApproved = (isAdmin) => {
  if(isAdmin){
    return Order.find({status: 'APPROVED'}).then(result => {return result});
  }

  let message = Promise.resolve('User must be an admin to access this!');
  return message.then((value) => {
    return {value};
  });
}

// delete an order by orderId
module.exports.deleteOrder = async (data) => {
  if (data.isAdmin) {
    return 'User must NOT be an admin to access this!';
  }

  // find and delete order by userId and orderId
  const result = await Order.deleteOne({ userId: data.userId, _id: data.body.orderId });

  if (result.deletedCount === 0) {
    return false;
  } else {
    return true;
  }
};

// Cancel an order 
module.exports.declineOrder = async (data) => {
  if(data.isAdmin){
    const result = await Order.findByIdAndDelete(data.orderId);

    if(result.deletedCount === 0){
      return false
    }else{
      return true;
    }
  }
}

// Approve order
module.exports.approveOrder = async (data) => {
  if (data.isAdmin) {
    try {
      await Order.findByIdAndUpdate({ _id: data.orderId }, { status: 'APPROVED' });
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  } else {
    return false;
  }
}
