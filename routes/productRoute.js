// Dependencies
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// Route to Create product (Admin only)
router.post("/admin/create", auth.verify, (req, res) => {

	const data = {
		body: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route to retrieve all active products
router.get("/all", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route to retrieve all products
router.get("/allAdmin", (req, res) => {
	productController.getAll().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving single product 
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating product information (Admin only)
router.patch("/update/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.params,
		update: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for archiving product (Admin only)
router.post("/admin/archive", auth.verify, (req, res) => {
	const data = {
		productId: req.body.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});




// export route
module.exports = router;