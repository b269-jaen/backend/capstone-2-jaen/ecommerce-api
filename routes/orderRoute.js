// Dependencies
const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");


// Route for creating order
router.post("/create", auth.verify, (req, res) => {

	const data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	body: req.body
	}

	orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
})


// Route for retrieving authenticated users orders
router.get("/user", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)

	orderController.getUserOrder(user).then(resultFromController => res.send(resultFromController));
})




// Retrieve all orders (Admin only)
router.get("/admin/all", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	orderController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Retrieve pending orders
router.get("/admin/pending", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	orderController.getPending(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Retrieve approved orders
router.get("/admin/approved", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	orderController.getApproved(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Delete an order by order id and userId 
router.delete("/cancel", auth.verify, (req, res) => {
	const data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	body: req.body
	}

	orderController.deleteOrder(data).then(resultFromController => res.send(resultFromController));
})

// decline an order (admin)
router.delete("/admin/cancel", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		orderId: req.body.orderId
	}

	orderController.declineOrder(data).then(resultFromController => res.send(resultFromController));
})

// Approve an order
router.post("/admin/approve", auth.verify, (req, res) => {
	const data ={ 
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		orderId: req.body.orderId
	}

	orderController.approveOrder(data).then(resultFromController => res.send(resultFromController));
})


// export route
module.exports = router;