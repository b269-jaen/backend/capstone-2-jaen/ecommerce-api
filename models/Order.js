const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String
	},
	products: [{
		productId: {type: String},
		productName: {type: String},
		quantity: {type: Number, default: 1},
		subTotal: {type: Number},
		img: {type: String}
	}
	],
	totalAmount: {type: Number},
	purchasedOn: {type: Date, default: new Date()},
	status: {type: String, default: 'PENDING'}
});

module.exports = mongoose.model("Order", orderSchema);