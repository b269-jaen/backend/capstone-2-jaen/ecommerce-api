const Product = require("../models/Product");
const mongoose = require("mongoose");


// creating product (Admin only)
module.exports.createProduct = (data) => {

	// if auth decode isAdmin: true, create new product
	if(data.isAdmin){
		let newProduct = new Product({
			name: data.body.name,
			description: data.body.description,
			price: data.body.price,
			note: data.body.note,
			img: data.body.img
		});

		// save Product
		return newProduct.save().then((product, error) =>{
			if(error){
				return false
			}else{
				return true
			}
		});
	};

	// else if auth decode isAdmin: false, return resolve message
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};


// Retrieve all active products 
module.exports.getAllActive = () => {
	// return all products with isActive is equals to true
	return Product.find({isActive: true}).then(result => {
		return result;
	})
};

// Retrieve all products 
module.exports.getAll = () => {
	// return all products 
	return Product.find().then(result => {
		return result;
	})
};

// Retrieve single product
module.exports.getProduct = (reqParams) =>{
	// find Product by params.id
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update Product information (Admin only)
module.exports.updateProduct = (data) =>{
	// if auth key isAdmin equals to true, update Product from req.Body
	if(data.isAdmin){
		let newUpdate = {
			name: data.update.name,
			description: data.update.description,
			price: data.update.price,
			note: data.update.note,
			img: data.update.img,
			isActive: data.update.isActive
		}
		// find by id and update
		return Product.findByIdAndUpdate(data.product.productId, newUpdate).then((update, err) => {
			if(err){
				return false
			}else{
				return true
			}
		});
	};

	// else if isAdmin equals to false, return resolve message
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

// Archive product (Admin only)
module.exports.archiveProduct = (data) => {

	// if auth key isAdmin equals true, find by id and update
	if(data.isAdmin){
		// set isActive field to false to archive product
		return Product.findByIdAndUpdate(data.productId, {isActive: false}).then((update, err) => {
			if(err){
				return false
			}else{
				return true
			};
		});
	};

	// else if isAdmin equals to false, return resolve message
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return false;
	});

	
};